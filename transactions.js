class Transaction {
    constructor(db, trx = null) {
        this.db = db
        this.trx = trx || null
        this.trxFlag = false
    }

    // Transaction Start Method
    async start() {
        if ((!this.trxFlag && !this.trx)) {
            // Initilizing Transaction Provider
            this.trxProvider = this.db.transactionProvider()
            this.trx = await this.trxProvider()
            this.trxFlag = true
        }
    }

    // Service Method
    async service() { }

    // DB Query Method
    knex(tableName) {
        if (this.trx) {
            return this.db(tableName).transacting(this.trx)
        }

        return this.db(tableName)
    }

    // DB Query Raw Method
    query(sql, params) {
        if (this.trx) {
            return this.db.raw(sql, params).transacting(this.trx)
        }

        return this.db.raw(sql, params)
    }

    batchInsert(tableName, data) {
        if (this.trx) {
            return this.db.batchInsert(tableName, data).transacting(this.trx)
        }

        return this.db.batchInsert(tableName, data)
    }

    // Commit Transaction
    commit() {
        if (this.trxFlag) {
            this.trx.commit()
        }
    }

    // Rollback Transaction
    rollback() {
        if (this.trxFlag) {
            this.trx.rollback()
        }
    }
}

module.exports = {
    Transaction
}